import React, { Component } from 'react';
import Header from './components/Header';
import Landing from './components/Landing';
import Dashboard from './components/Dashboard';
import { Route, HashRouter } from "react-router-dom";
import * as routes from './constants/routes';
import Home from './components/home';
import { firebase } from './firebase';
import withAuthentication from './components/withAuthentication';


class Main extends Component {
    render() {
        return (
            <HashRouter>
                <div className="Main">
                    <Header />
                    <div className="content">
                        <Route exact path="/" component={Landing} />
                        <Route path={routes.LANDING} component={Landing} />
                        <Route path={routes.DASHBOARD} component={Dashboard} />
                        <Route path={routes.HOME} component={Home} />
                    </div>
                </div>
            </HashRouter>
        );
    }
}

export default Main;
