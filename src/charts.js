import $ from 'jquery';
import Cookies from 'js-cookie';
import Chart from 'chart.js';

function processCookie(data) {
    data = Cookies.get(data.toString());
    data = data.substring(1, data.length - 1)
    data = data.split(",");
    return data;
}

export default function renderGraphs() {
    // Most Popular Chart tile 1
    var dataMostPopular = {
        labels: ['IPA', 'Porter', 'Pils'],
        datasets: [{
            label: "My first",
            data: processCookie("dataMostPopularVals"),
            backgroundColor: [
                "rgb(76, 204, 185, 0.7)",
                "rgb(54, 162, 235, 0.7)",
                "rgb(255, 231, 145, 0.7)"
            ]
        }]
    };

    var ctxMostPopular = $("#mostPopular");
    new Chart(ctxMostPopular, {
        type: 'pie',
        data: dataMostPopular,
    });

    // Amount vs prive bar and normal tile 2
    var dataAmountVsPrice = {
        labels: [
            "(0;5>",
            "(5,7>",
            "(7;9>",
            "(9;11>",
            "(11;13>",
            "(13;15>",
            "(15;17>",
            "(17;ing)"
        ],
        datasets: [{
            label: "Amount vs price",
            data: processCookie("dataAmountVsPriceVals"),
            backgroundColor: [
                "rgba(255, 99, 132, 0.2)",
                "rgba(255, 159, 64, 0.2)",
                "rgba(255, 205, 86, 0.2)",
                "rgba(75, 192, 192, 0.2)",
                "rgba(54, 162, 235, 0.2)",
                "rgba(153, 102, 255, 0.2)",
                "rgba(201, 203, 207, 0.2)"
            ],
            borderColor: [
                "rgb(255, 99, 132)",
                "rgb(255, 159, 64)",
                "rgb(255, 205, 86)",
                "rgb(75, 192, 192)",
                "rgb(54, 162, 235)",
                "rgb(153, 102, 255)",
                "rgb(201, 203, 207)"
            ],
            borderWidth: 1
        }]
    }

    var ctxAmountVsPrice = $("#amountVsPrice");
    new Chart(ctxAmountVsPrice, {
        type: "bar",
        data: dataAmountVsPrice,
        options: {
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: "Price range",
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: "Units sold",
                    }
                }]
            }
        }
    });

    //  Tap vs bottle tile 3
    var dataTapVsBottle = {
        labels: ["Tap", "Bottle"],
        datasets: [{
            label: "Tap vs bottle",
            data: processCookie("dataTapVsBottle"),
            backgroundColor: [
                "rgb(255, 99, 132, 0.7)",
                "rgb(54, 162, 235, 0.7)"
            ]
        }]
    };

    var ctxTapVsBottle = $("#tapVsBottle");
    new Chart(ctxTapVsBottle, {
        type: 'pie',
        data: dataTapVsBottle
    });

    // Best days bar chart tile 8
    var dataBestDays = {
        labels: [
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thurstday",
            "Friday",
            "Saturday",
            "Sunday"
        ],
        datasets: [{
            label: "Best days",
            data: processCookie("dataBestDaysVals"),
            backgroundColor: [
                "rgba(255, 99, 132, 0.2)",
                "rgba(255, 159, 64, 0.2)",
                "rgba(255, 205, 86, 0.2)",
                "rgba(75, 192, 192, 0.2)",
                "rgba(54, 162, 235, 0.2)",
                "rgba(153, 102, 255, 0.2)",
                "rgba(201, 203, 207, 0.2)"
            ],
            borderColor: [
                "rgb(255, 99, 132)",
                "rgb(255, 159, 64)",
                "rgb(255, 205, 86)",
                "rgb(75, 192, 192)",
                "rgb(54, 162, 235)",
                "rgb(153, 102, 255)",
                "rgb(201, 203, 207)"
            ],
            borderWidth: 1
        }]
    };

    var ctxBestDays = $("#bestDays");
    new Chart(ctxBestDays, {
        type: "bar",
        data: dataBestDays,
        options: {
            scales: {
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: "Average income"
                    }
                }]
            }
        }
    });

    // Monthly income line chart tile 9
    var dataMonthlyIncome = {
        labels: [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July"
        ],
        datasets: [{
            label: "Monthly income",
            fill: false,
            data: processCookie("dataMonthlyIncomeVals"),
            borderColor: "rgb(255, 120, 255, 0.7)"
        }, {
            label: "Monthly outcome",
            data: processCookie("dataMonthlyOutcomeVals"),
            borderColor: "rgb(125, 232, 232)"
        }]
    }

    var ctxMonthlyIncome = $("#monthlyIncome");
    new Chart(ctxMonthlyIncome, {
        type: "line",
        data: dataMonthlyIncome,
        options: {
            scales: {
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: "Amount"
                    }
                }]
            }
        }
    });
}
