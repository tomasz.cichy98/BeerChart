import React, { Component } from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';
import { NavLink as RRNavlink } from 'react-router-dom';
import LogOutButton from './home/LogOutButton';
import NavbarLinks from './dashboard/NavbarLinks';

const LogOut = ({ authUser }) => { authUser ? <LogOutButton /> : "" }

class Header extends Component {
    constructor() {
        super();
        this.toggleNavbar = this.toggleNavbar.bind(this);
        this.state = {
            isOpen: true,
        }
    }

    toggleNavbar() {
        this.setState({
            isOpen: !this.state.isOpen,
        });
    }

    render() {
        return (
            <div>
                <Navbar dark className="fiexdTop" expand="md">
                    <NavbarBrand href="/" className="mr-auto" style={{ color: 'white' }}>BeerChart</NavbarBrand>
                    <NavbarToggler onClick={this.toggleNavbar} className="mr-2" />
                    <Collapse isOpen={!this.state.isOpen} navbar>
                        <NavbarLinks authUser = {this.props.authUser}/>
                    </Collapse>
                </Navbar>
            </div>
        );
    }
}

export default Header;
