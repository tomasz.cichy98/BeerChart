import React, { Component } from 'react';
import '../styles/Landing.css';
import Features from './landing/Features';
import Pricing from './landing/Pricing';
import Welcome from './landing/Welcome';

class Landing extends Component {
    render() {
        return (
            <div className="Landing">
                <Welcome />
                <Features />
                <Pricing />
            </div>
        );
    }
}

export default Landing
