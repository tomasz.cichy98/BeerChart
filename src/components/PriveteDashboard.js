import React, { Component } from 'react';
import renderGraphs from '../charts';
import { Container } from 'reactstrap';
import TopRow from './dashboard/TopRow';
import BottomRow from './dashboard/BottomRow';
import '../data';

class Dashboard extends Component {
    componentDidMount() {
        renderGraphs();
    }
    
    render() {
        return (
            <div className="Dashboard">
                <Container fluid>
                   <TopRow />
                   <BottomRow />
                </Container>
            </div>
        );
    }
}

export default Dashboard;
