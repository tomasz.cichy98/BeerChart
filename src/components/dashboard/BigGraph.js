import React, { Component } from 'react';

import { Col } from 'reactstrap';


class BigGraph extends Component {
    render() {
        return (
            <Col md="6" xl="6">
                <canvas id={this.props.id} width="820" height="400"></canvas>
            </Col>
        );
    }
}

export default BigGraph;
