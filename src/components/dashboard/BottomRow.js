import React, { Component } from 'react';

import { Row } from 'reactstrap';
import BigGraph from './BigGraph';


class BottomRow extends Component {
    render() {
        return (
            <Row id="bottom_row">
                <BigGraph id="bestDays" />
                <BigGraph id="monthlyIncome" />
            </Row>
        );
    }
}

export default BottomRow;