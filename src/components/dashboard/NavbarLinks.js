import React, { Component } from 'react';
import { Nav, NavItem, NavLink, Form } from 'reactstrap';
import { NavLink as RRNavlink } from 'react-router-dom';
import LogOutButton from '../home/LogOutButton';
import AuthUserContext from '../AuthUserContext';

const NavbarLinks = ({ authUser }) =>
    <div>
        {authUser ? <NavAuth /> : <NavNoAuth />}
    </div>


const NavAuth = () =>
    <Nav className="ml-auto" navbar>
        <NavItem>
            <NavLink to="/Landing" tag={RRNavlink}>Landing</NavLink>
        </NavItem>
        <NavItem>
            <NavLink to="/Dashboard" tag={RRNavlink}>Dashboard</NavLink>
        </NavItem>

        <NavItem>
            <LogOutButton />
        </NavItem>
    </Nav>

const NavNoAuth = () =>
    <Nav className="ml-auto" navbar>
        <NavItem>
            <NavLink to="/Landing" tag={RRNavlink}>Landing</NavLink>
        </NavItem>
        <NavItem>
            <NavLink to="/Dashboard" tag={RRNavlink}>Dashboard</NavLink>
        </NavItem>
    </Nav>

export default NavbarLinks;
