import React, { Component } from 'react';

import { Col, Row} from 'reactstrap';


class NumericVals extends Component {
    render() {
        return (
            <Col md="6" xl="3">
                <span className="box">
                    <Row id="top_small_row">
                        <Col className="small_box text-center">Text1</Col>
                        <Col className="small_box text-center">Text2</Col>
                    </Row>
                    <Row id="bottom_small_row">
                        <Col className="small_box text-center">Text3</Col>
                        <Col className="small_box text-center">Text4</Col>
                    </Row>
                </span>
            </Col>
        );
    }
}

export default NumericVals;
