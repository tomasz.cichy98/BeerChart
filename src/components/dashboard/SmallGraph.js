import React, { Component } from 'react';

import { Col } from 'reactstrap';


class SmallGraph extends Component {
    render() {
        return (
            <Col md="6" xl="3">
                <canvas id={this.props.id} width="400" height="400"></canvas>
            </Col>
        );
    }
}

export default SmallGraph;
