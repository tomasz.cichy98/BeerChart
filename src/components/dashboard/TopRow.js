import React, { Component } from 'react';

import { Row } from 'reactstrap';
import SmallGraph from './SmallGraph';
import NumericVals from './NumericVals';


class TopRow extends Component {
    render() {
        return (
            <Row id="top_row">
                <SmallGraph id="mostPopular" />
                <SmallGraph id="amountVsPrice" />
                <SmallGraph id="tapVsBottle" />
                <NumericVals />
            </Row>
        );
    }
}

export default TopRow;
