import React from 'react';
import { Button } from 'reactstrap';
import { auth } from '../../firebase';

const LogOutButton = () =>
    <Button onClick={auth.doSignOut} color="primary" className="ml-auto btn-sm" outline>Log Out</Button>
export default LogOutButton;
