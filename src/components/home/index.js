import React, { Component } from 'react';
import PrivateDashboard from '../PriveteDashboard';
import '../../data';

class Home extends Component {
    
    render() {
        return (
            <div className="home">
            <PrivateDashboard />
            </div>
        );
    }
}

export default Home;
