import React, { Component } from 'react';
import '../../styles/Landing.css';
import { Container, Row, Col } from 'reactstrap';


class Features extends Component {
    render() {
        return (
            <Container fluid>
                <Row className="why_us_row justify-content-center" id="features">
                    <Col md="3" className="text-center">
                        <h3>Icon:</h3>
                        <p>Lorem ipsum dolor amet sustainable meditation flexitarian asymmetrical, disrupt bespoke plaid shamanlocavore. Venmo dreamcatcher edison blib leggings street art gentrify.</p>
                    </Col>
                    <Col md="3" className="text-center">
                        <h3>Icon:</h3>
                        <p>Lorem ipsum dolor amet sustainable meditation flexitarian asymmetrical, disrupt bespoke plaid shamanlocavore. Venmo dreamcatcher edison blib leggings street art gentrify.</p>
                    </Col>
                    <Col md="3" className="text-center">
                        <h3>Icon:</h3>
                        <p>Lorem ipsum dolor amet sustainable meditation flexitarian asymmetrical, disrupt bespoke plaid shamanlocavore. Venmo dreamcatcher edison blib leggings street art gentrify.</p>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default Features;
