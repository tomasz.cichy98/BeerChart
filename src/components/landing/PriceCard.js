import React, { Component } from 'react';
import { Card, CardBody, CardTitle, CardHeader, CardText } from 'reactstrap';


class PriceCard extends Component {
    render() {
        return (
            <Card>
                <CardBody>
                    <CardHeader className="text-center">{this.props.header}</CardHeader>
                    <CardTitle className="text-center pricing-card-title">{this.props.title}</CardTitle>
                    <CardText className="text-center">{this.props.text}</CardText>
                </CardBody>
            </Card>
        );
    }
}

export default PriceCard;
