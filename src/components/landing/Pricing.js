import React, { Component } from 'react';
import '../../styles/Landing.css';
import { Container, Row, CardDeck } from 'reactstrap';
import PriceCard from './PriceCard';


class Pricing extends Component {
    render() {
        return (
            <Container fluid>
                <Row className="justify-content-center" id="pricing">
                    <CardDeck className="justify-content-center">
                        <PriceCard header="Trial" title="$0 /mo" text="No trzeba to naprawić" />
                        <PriceCard header="Basic" title="$200 /mo" text="No trzeba to naprawić" />
                        <PriceCard header="Enterprise" title="$999 /mo" text="No trzeba to naprawić" />
                    </CardDeck>
                </Row>
            </Container>
        );
    }
}

export default Pricing;
