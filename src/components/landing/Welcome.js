import React, { Component } from 'react';
import '../../styles/Landing.css';
import { Container, Row, Col } from 'reactstrap';
import LoginModal from './modals/LoginModal';
import RegisterModal from './modals/RegisterModal';

class Welcome extends Component {
  render() {
    return (
      <Container fluid>
        <Row className="welcome_row justify-content-center" id="abouot">
          <Col md='6'>
            <h1 className="text-center">Welcome to Beer Chart</h1>
            <p className="text-justify">Beer Chart is Lorem ipsum dolor amet unicorn leggings readymade quinoa stumptown bitters single-origin
                    coffee. Farm-to-table raw denim cloud bread etsy. Shoreditch lomo pop-up taiyaki tumblr enamel pin
                    keffiyeh tacos hot chicken live-edge knausgaard pinterest. Williamsburg quinoa literally, unicorn
                        jean shorts taiyaki listicle pug man bun yuccie small batch tote bag pabst.</p>
              <div className="d-flex justify-content-center">
              <LoginModal buttonLabel="Login" />
              <RegisterModal buttonLabel="Register" />
              </div>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Welcome;
