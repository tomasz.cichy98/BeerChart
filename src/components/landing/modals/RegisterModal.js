import React from 'react';
import { Button, Modal, ModalHeader, ModalBody } from 'reactstrap';
import RegisterForm from './forms/RegisterForm';

class RegisterModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }
  render() {
    return (
      <div>
        <Button color="primary" className="btn-welcome" onClick={this.toggle}>{this.props.buttonLabel}</Button>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}>Register</ModalHeader>
          <ModalBody>
            <RegisterForm onSubmit={this.handleSignUp} />
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

export default RegisterModal;
