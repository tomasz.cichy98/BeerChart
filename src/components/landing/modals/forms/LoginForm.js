import React from 'react';
import { withRouter } from 'react-router-dom';
import { Button, Form, FormGroup, Input, Label } from 'reactstrap';
import { auth } from '../../../../firebase';
import * as routes from '../../../../constants/routes';

const LoginModal = ({ history }) =>
  <div>
    <LoginForm history={history} />
  </div>

const INITIAL_STATE = {
  email: '',
  password: '',
  error: null,
};

const byPropKey = (propertyName, value) => () => ({
  [propertyName]: value,
});


class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { ...INITIAL_STATE };
  }

  onSubmit = (event) => {
    const {
      email, password,
    } = this.state;

    const { history } = this.props;
    auth.doSignInWithEmailAndPassword(email, password)
      .then(() => {
        this.setState(() => ({ ...INITIAL_STATE }));
        history.push(routes.HOME);
      })
      .catch(error => {
        this.setState(byPropKey('error', error));
      });

    event.preventDefault();
  }

  render() {
    const { email, password, error } = this.state;
    const isInvalid = password === '' || email === '';

    return (
      <Form onSubmit={this.onSubmit}>
        <FormGroup>
          <Label for="email">Email</Label>
          <Input name="email" label="Email Address" type="email" value={email} onChange={event => this.setState(byPropKey('email', event.target.value))} required/>
        </FormGroup>
        <FormGroup>
          <Label for="passwordOne">Password</Label>
          <Input name="passwordOne" label="passwordOne" type="password" onChange={event => this.setState(byPropKey('password', event.target.value))} required />
        </FormGroup>
        <Button color="primary" disabled={isInvalid}>Submit</Button>

        {error && <p>{error.message}</p>}
      </Form>
    );
  }
}

export default withRouter(LoginModal);

export {LoginForm};