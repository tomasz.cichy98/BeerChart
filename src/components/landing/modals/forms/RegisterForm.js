import React from 'react';
import { Button, Form, Label, Input, FormGroup } from 'reactstrap';
import { auth } from '../../../../firebase';

const INITIAL_STATE = {
    username: '',
    email: '',
    passwordOne: '',
    passwordTwo: '',
    error: null,
}

const byPropKey = (propertyName, value) => () => ({
    [propertyName]: value,
});

class RegisterForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = { ...INITIAL_STATE };
    }

    onSubmit = (event) => {
        event.preventDefault();

        const {
            username,
            email,
            passwordOne
        } = this.state;

        const {
            history
        } = this.props;

        auth.doCreateUserWithEmailAndPassword(email, passwordOne).then(authUser => {
            console.log("user created");

            this.setState(() => ({ ...INITIAL_STATE }));
        }).catch(error => {
            console.log("error while creatig user");

            this.setState(byPropKey('error', error));
        });

    }

    render() {
        const {
            username,
            email,
            passwordOne,
            passwordTwo,
            error,
        } = this.state;

        const isInvalid =
            passwordOne !== passwordTwo ||
            passwordOne === '' ||
            email === '' ||
            username === '';


        return (
            <div>
                <Form onSubmit={this.onSubmit}>
                    <FormGroup>
                        <Label for="username">username</Label>
                        <Input name="username" label="username" type="username" value={username} onChange={event => this.setState(byPropKey('username', event.target.value))} required />
                    </FormGroup>
                    <FormGroup>
                        <Label for="email">Email</Label>
                        <Input name="email" label="Email Address" type="email" value={email} onChange={event => this.setState(byPropKey('email', event.target.value))} required />
                    </FormGroup>
                    <FormGroup>
                        <Label for="passwordOne">Password</Label>
                        <Input name="passwordOne" label="passwordOne" type="password" onChange={event => this.setState(byPropKey('passwordOne', event.target.value))} required />
                    </FormGroup>
                    <FormGroup>
                        <Label for="passwordTwo">Confirm Password</Label>
                        <Input name="passwordTwo" label="passwordTwo" type="password" onChange={event => this.setState(byPropKey('passwordTwo', event.target.value))} required />
                    </FormGroup>
                    <Button color="primary" disabled={isInvalid}>Submit</Button>

                    {error && <p>{error.message}</p>}
                </Form>
            </div>
        );
    }
}

export default RegisterForm;