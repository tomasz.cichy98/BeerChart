export const LANDING = '/Landing';
export const DASHBOARD = "/Dashboard";
export const HOME = '/components/home';
export const ACCOUNT = '/account';
export const PASSWORD_FORGET = '/pw-forget';