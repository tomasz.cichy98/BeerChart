import Cookies from 'js-cookie';
import $ from 'jquery';

// Most Popular
var dataMostPopularVals = [100, 54, 72];
Cookies.set("dataMostPopularVals", dataMostPopularVals);

// Hist
var dataAmountVsPriceVals = [5, 6, 3, 4, 6, 2, 6, 8];
Cookies.set("dataAmountVsPriceVals", dataAmountVsPriceVals);

// Tap vs bottle
var dataTapVsBottleVals = [54335, 4325];
Cookies.set("dataTapVsBottle", dataTapVsBottleVals);

// Best days
var dataBestDaysVals = [643, 653, 325, 76, 423, 431, 458];
Cookies.set("dataBestDaysVals", dataBestDaysVals);

// Monthly income
var dataMonthlyIncomeVals = [3, 7, 0, 3, 6, 9, 12];
Cookies.set("dataMonthlyIncomeVals", dataMonthlyIncomeVals);
var dataMonthlyOutcomeVals = [5, 2, 4, 6, 1, 3, 1];
Cookies.set("dataMonthlyOutcomeVals", dataMonthlyOutcomeVals);

// Parse JSON
// var date;
// var value;
// var datesValsIn = [];
// var datesValsOut = [];

// var json = $.getJSON("../data/test.json", function (data) {
//     var items = [];
//     $.each(data, function (index, pval) {
//         // loop parnets
//         $.each(pval, function (key, val) {

//             // create date yyyy month
//             date = val["year"] + " " + val["month"];

//             // chech if income or outcome
//             if (index === "monthlyIncome") {
//                 value = val["amount"];
//                 // create array of arrays income
//                 datesValsIn.push([date, value]);
//             } else if (index === "monthlyOutcome") {
//                 value = val["amount"];
//                 // create array of arrays outcome
//                 datesValsOut.push([date, value]);

//             }
//             $.each(val, function (nkey, nval) {
//                 // each parents data
//             })
//         });
//     });
// });

// console.log(datesValsIn);
// console.log(datesValsOut);
// // /Parse JSON