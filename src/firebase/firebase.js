import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

const config = {
    apiKey: "AIzaSyCKx7kjMOdmL04NQKDfQmUwljbCqHUKF50",
    authDomain: "beer-chart.firebaseapp.com",
    databaseURL: "https://beer-chart.firebaseio.com",
    projectId: "beer-chart",
    storageBucket: "beer-chart.appspot.com",
    messagingSenderId: "380517524103"
};

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

const db = firebase.database();
const auth = firebase.auth();

export {
  db,
  auth,
};